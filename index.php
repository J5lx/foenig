<?php

if (isset($_POST['text']) && !empty($_POST['text'])) {
    $text = $_POST['text'];

    if (isset($_POST['extended-mode']) && $_POST['extended-mode'])
        $extendedMode = true;
    else
        $extendedMode = false;

    // Um nicht bereits ersetzte Fs zurückzusetzen, ersetzen wir sie erst durch
    // ein Spezialzeichen, dass wir zum Schluss dann jeweis durch ein K ersetzen
    $text = str_replace(array('F', 'f'), array("\x1A", "\x1B"), $text);
    $text = str_replace(array('K', 'k'), array("F", "f"), $text);
    $text = str_replace(array("\x1A", "\x1B"), array('K', 'k'), $text);

    if ($extendedMode)
        $text = str_replace(array('B', 'G', 'D', 'b', 'g', 'd'),
                            array('P', 'K', 'T', 'p', 'k', 't'),
                            $text);
}

?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title>Der F&ouml;nigisierer &ndash; Ersetze in deinem Text das F durch das K und umgekehrt</title>
        <link rel="stylesheet" href="css/semantic.min.css">
        <style>
            body {
                font-family: "Open Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
            }
        </style>
    </head>
    <body>
        <div class="ui page grid">
            <div class="column">
                <h1>Der F&ouml;nigisierer &ndash; Ersetze in deinem Text das F durch das K und umgekehrt</h1>

                <?php if(isset($text)): ?>

                    <div class="ui green icon message">
                        <i class="checkmark icon"></i>
                        <div class="content">
                            <div class="header">
                                Hier ist das Ergebnis!
                            </div>
                            <p>
                                Dein Text wurde soeben f&ouml;nigisiert. Hier ist das Ergebnis:
                            </p>
                            <div class="ui form">
                                <div class="field">
                                    <textarea readonly><?=htmlentities($text)?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>

                <form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
                    <div class="ui form">
                        <div class="field">
                            <label>Dein Text:</label>
                            <textarea name="text" required></textarea>
                        </div>
                        <div class="inline field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="extended-mode">
                                <label>Erweitertes F&ouml;nigisieren (Ersetze auch B durch P, G durch K und D durch T)</label>
                            </div>
                        </div>
                        <input type="submit" class="ui large green button" value="F&ouml;nigisieren">
                    </div>
                </form>

                <h2>&Uuml;ber den F&ouml;nigisierer</h2>
                <p>
                    Der F&ouml;nigisierer basiert auf dem Buch &bdquo;Der
                    F&ouml;nig&rdquo; von Walter Moers, das in einem
                    K&ouml;nigreich spielt, in dem das F immer wie ein K und das
                    K wie ein F ausgesprochen werden muss. Sp&auml;ter soll der
                    K&ouml;nig f&uuml;r ein h&auml;ssliches M&auml;nnlein auch
                    noch das B wie ein P, das G wie ein K und das D wie ein T
                    sprechen. Wie es dann zu diesem Tool kam, erkl&auml;rt sich
                    von selbst.
                </p>

                <footer>
                    <small>
                        Geschrieben von <a href="//j5lx.eu/" target="_blank">J5lx</a>
                        in <a href="//php.net/" target="_blank">PHP</a>
                        und <a href="//w3.org/html/" target="_blank">HTML</a> unter
                        Verwendung von
                        <a href="//semantic-ui.com/" target="_blank">Semantic UI</a>.
                        Quellcode unter den Bedingungen der
                        <a href="//gnu.org/licenses/agpl-3.0.html" target="_blank">GNU AGPL v3</a>
                        <a href="//github.com/J5lx/foenig/" target="_blank">auf GitHub</a>
                        verf&uuml;gbar.
                    </small>
                </footer>
            </div>
        </div>
        <script src="javascript/jquery.min.js"></script>
        <script src="javascript/semantic.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.ui.checkbox')
                    .checkbox()
                ;
            });
        </script>
    </body>
</html>
