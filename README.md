foenig
======

---

If you don’t understand German you can beat me but it won’t change anything since this tool is based on a german book which probably can’t be translated properly into any other language.

---

Der Fönigisierer ersetzt in Texten jedes F durch ein K und umgekehrt – Ganz wie in „Der Fönig“ eben.
Nebenbei bemerkt: Alternative (bessere) Namensvorschläge werden gerne entgegengenommen.
